from django.conf.urls import patterns, include, url
from views import RegistrationView

urlpatterns = patterns('',
       url(r'^register/$', RegistrationView.as_view(), name="account_register"),
       url(r'^login/$', 'django.contrib.auth.views.login', {'template_name':'registration/login.html'}),
       )
