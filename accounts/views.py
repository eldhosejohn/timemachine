from django.shortcuts import render_to_response, render, redirect
from django.http import HttpResponse
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import login
 
from forms.registration import RegistrationForm
from django.views.generic.base import View

from django.conf import settings

def home(request):
    if request.user.is_authenticated():
        return redirect("/services")
    else:
        register_form = RegistrationForm()
        return render_to_response('home/home.html', {'form':register_form}, context_instance=RequestContext(request))

class RegistrationView(View):
    def get(self, request):
         register_form = RegistrationForm()
         return render_to_response('registration/registration.html', {'form':register_form}, context_instance=RequestContext(request))
    
    def post(self, request):
        register_form = RegistrationForm(data=request.POST)
        
        if register_form.is_valid():
            user = register_form.save()
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
            return redirect("/delicious/")
        
        return render_to_response('registration/registration.html', {'form':register_form}, context_instance=RequestContext(request))
