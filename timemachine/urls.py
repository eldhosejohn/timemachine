from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'timemachine.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^delicious/', include('delicious.urls')),
    url(r'^accounts/', include('accounts.urls')),


    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^$', include('timemachineapp.urls')),


)
