"""
Django settings for timemachine project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'a3rv^upsvjm*4&m-$tlvd6ga@cq9t)x%h%s2s$qa8h46&sn9n&'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'timemachineapp',
    'social.apps.django_app.default',
    'delicious',
    'djcelery',
    'mail_templated',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)


ROOT_URLCONF = 'timemachine.urls'

WSGI_APPLICATION = 'timemachine.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'timemachine',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}






# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'templates'),
)


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'social.backends.instagram.InstagramOAuth2',
    'timemachineapp.delicious.DeliciousOAuth2',
    'accounts.backends.EmailAuthBackend',

)

## Social Params

SOCIAL_AUTH_FLICKR_KEY = 'ec4113af80fb16529c6ab525b9025b6b'
SOCIAL_AUTH_FLICKR_SECRET = 'fa0d2315018d7998'


SOCIAL_AUTH_INSTAGRAM_KEY ='8430c07c01eb4dc4838cfd56c43ef782'
SOCIAL_AUTH_INSTAGRAM_SECRET = '6fb6179c723945df870a91ae33becc28'

SOCIAL_AUTH_DELICIOUS_KEY = '4ddb62c901385d1fc7d29cd9e7d83bf1'
SOCIAL_AUTH_DELICIOUS_SECRET = 'f7a609ce9d4867ad0d63ca3a26110b1a'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'yelloweb@gmail.com'
EMAIL_HOST_PASSWORD = 'brilliant123#'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

LOGIN_REDIRECT_URL = '/'