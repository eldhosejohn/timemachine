__author__ = 'eldhosejohn'

from social.backends.oauth import BaseOAuth2
from requests import HTTPError
from social.exceptions import AuthFailed, AuthCanceled, AuthUnknownError, \
                              AuthMissingParameter, AuthStateMissing, \
                              AuthStateForbidden, AuthTokenError

2
class DeliciousOAuth2(BaseOAuth2):
    "Delicious OAuth backend"
    name = "delicious"
    AUTHORIZATION_URL = 'https://delicious.com/auth/authorize'
    ACCESS_TOKEN_URL = 'https://avosapi.delicious.com/api/v1/oauth/token'
    ACCESS_TOKEN_METHOD = 'POST'
    REDIRECT_STATE = False

    STATE_PARAMETER = False
    ACCESS_TOKEN_METHOD = 'POST'


    def do_auth(self, access_token, response=None, *args, **kwargs):
        response = response or {}
        data = self.user_data(access_token)

        data['access_token'] = response.get('access_token')
        kwargs.update({'backend': self, 'response': data})
        return self.strategy.authenticate(*args, **kwargs)

    def get_user_details(self, response):
        return "3423423"

    def user_data(self, access_token, *args, **kwargs):
        """Loads user data from service"""
        #url = 'https://api.github.com/user?' + urlencode({
        #    'access_token': access_token
        #})
        #try:
        #    return json.load(self.urlopen(url))
        #except ValueError:
        #    return None
        #return "23123"
        response = {
                'user_id': '',
            }
        return response
