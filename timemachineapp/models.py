from django.db import models
from django.contrib.auth.models import User

class Bookmark(models.Model):
    description = models.TextField()
    date = models.DateTimeField()
    url = models.CharField(max_length=200)
    user = models.ForeignKey(User)


