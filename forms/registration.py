from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm, ValidationError
from random import choice
from string import letters

class RegistrationForm(UserCreationForm):
    """
    Registration with only email and one password
    """
    email = forms.EmailField(min_length=4, label=("Email"), max_length=75, required=True)
    
    class Meta:
        model = User
        fields =['email','password1']

    def __init__(self, user=None, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        del self.fields['username']
        del self.fields['password2']
    
    def clean_email(self):
        email = self.cleaned_data.get('email')
        try:
            User.objects.get(email__iexact=email)
        except User.DoesNotExist:
            return email.lower()
        raise forms.ValidationError("A user with the entered email already exists")

    def save(self):
        user = super(RegistrationForm, self).save(commit=False)
        random_username = 'user-' + ''.join([choice(letters) for i in xrange(10)])
        user.username = random_username
        
        user.save()
        return user
         
    
        
    