__author__ = 'eldhosejohn'
import requests
import datetime

from celery import shared_task
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User

from mail_templated import EmailMessage

from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import render_to_string


from delicious.models import DeliciousModel
from timemachineapp.models import Bookmark

@shared_task()
def download_bookmarks(user):
    delicious_model = DeliciousModel.objects.get(user=user)
    access_token = delicious_model.access_token

    headers_bearers = 'Bearer ' + str(access_token)
    headers = {'Authorization' : headers_bearers}
    r = requests.get('http://api.del.icio.us/v1/json/posts/all', headers=headers)
    response = r.json()

    posts = response["posts"]
    for post_obj in posts:
        post = post_obj['post']
        post_time = post['time']
        post_description = post['description']
        post_url = post['href']
        post_url_hash = post['hash']

        bookmark = Bookmark()
        bookmark.date = post_time
        bookmark.url = post_url
        bookmark.description = post_description
        bookmark.user = user
        bookmark.save()


@shared_task()
def send_email():
    users = User.objects.filter()
    today = datetime.datetime.now()

    for user in users:
        bookmarks = Bookmark.objects.filter(user=user, date__year='2014', date__month='01', date__day='25')
        if len(bookmarks) != 0:
            c = Context({'bookmarks': bookmarks})
            html_content = render_to_string('email/email_template.html', c)

            email = EmailMultiAlternatives('Lets travel back - TimeMachine', html_content)
            email.attach_alternative(html_content, "text/html")
            email.to = [user.email]
            email.send()





