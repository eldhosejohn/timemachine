__author__ = 'eldhosejohn'

from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       url(r'^$', 'delicious.views.authorize', name='home'),
                       url(r'^success/', 'delicious.views.success', name='success')
)
