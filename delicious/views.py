import requests
from django.http import HttpResponse
from models import DeliciousModel
from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from delicious.tasks import download_bookmarks

AUTHORIZATION_URL = 'https://delicious.com/auth/authorize'
ACCESS_TOKEN_URL = 'https://avosapi.delicious.com/api/v1/oauth/token'

def authorize(request):
    authorize_url =  AUTHORIZATION_URL + '?client_id=' + settings.SOCIAL_AUTH_DELICIOUS_KEY + '&redirect_uri=http://localtest.me/delicious/success/'
    return HttpResponseRedirect(authorize_url)


def success(request):
    code = request.GET.get('code')
    r = requests.post(ACCESS_TOKEN_URL, data={'client_id': settings.SOCIAL_AUTH_DELICIOUS_KEY, 'client_secret': settings.SOCIAL_AUTH_DELICIOUS_SECRET,
                                              'code': code, 'grant_type':'code'})
    response = r.json()
    access_token = response['access_token']
    try:
        delicious_model = DeliciousModel.objects.get(user=request.user)
    except DeliciousModel.DoesNotExist:
        delicious_model = DeliciousModel(user=request.user)

    delicious_model.access_token = access_token
    delicious_model.save()
    download_bookmarks(request.user)

    return HttpResponseRedirect("http://localtest.me")
