__author__ = 'eldhosejohn'


from django.db import models
from django.contrib.auth.models import User

class DeliciousModel(models.Model):
    user = models.ForeignKey(User)
    access_token = models.CharField(max_length=100)
